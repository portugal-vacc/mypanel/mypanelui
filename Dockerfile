FROM node:18.10-alpine as builder

WORKDIR /app
COPY package.json package-lock.json ./
RUN npm install
COPY . .
RUN npm run build

FROM nginx:1.15.8
EXPOSE 4200
COPY conf/default.conf /etc/nginx/conf.d/
RUN rm -rf /usr/share/nginx/html/*
COPY --from=builder /app/dist/mypanelui /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]
