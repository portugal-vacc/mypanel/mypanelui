import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { HttpService } from '../http.service';

@Component({
  selector: 'app-callback',
  templateUrl: './callback.component.html',
  styleUrls: ['./callback.component.scss']
})
export class CallbackComponent implements OnInit {


  constructor(private activatedRoute : ActivatedRoute, private AuthService : AuthService, private router: Router, private httpService : HttpService) {

  }
  ngOnInit(): void {
    if(this.AuthService.isLoggedIn()) {
      this.router.navigateByUrl('/dashboard');
      return;
    }
    const snapShot = this.activatedRoute.snapshot;

    const token = snapShot.queryParams['token'];

    if(token) {
      const promise = this.httpService.validateToken(token).toPromise();
      promise.then(() => {
        this.AuthService.storeToken(token);
        this.router.navigateByUrl('/dashboard');
      }).catch(() => {
          this.AuthService.logOut();
      });
    } else {
      this.AuthService.logOut();     
    }
  }

}
