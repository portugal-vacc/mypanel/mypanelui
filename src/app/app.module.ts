import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {ApproutingModule} from './app-routing.module';
import {HeaderComponent} from './header/header.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {UserprofileComponent} from './userprofile/userprofile.component';
import {NotfoundComponent} from './notfound/notfound.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LoginComponent} from './login/login.component';
import {MatMenuModule} from '@angular/material/menu';
import {SvgIconComponent} from './svg-icon/svg-icon.component';
import {AvatarComponent} from './avatar/avatar.component';
import {HttpClientModule} from '@angular/common/http';
import {MatExpansionModule} from '@angular/material/expansion';
import {CallbackComponent} from './callback/callback.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatButtonModule} from '@angular/material/button';
import { BookingManagerComponent } from './booking-manager/booking-manager.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { CalendarComponent } from './calendar/calendar.component';
import { NzTimePickerModule } from 'ng-zorro-antd/time-picker';
import { en_US, NZ_I18N } from 'ng-zorro-antd/i18n';
import { registerLocaleData } from '@angular/common';
import { NzInputModule } from 'ng-zorro-antd/input';
import en from '@angular/common/locales/en';

registerLocaleData(en);
import { MembersComponent } from './members/members.component';
import { SearchFilterPipe } from './search-filter.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DashboardComponent,
    UserprofileComponent,
    NotfoundComponent,
    LoginComponent,
    SvgIconComponent,
    AvatarComponent,
    CallbackComponent,
    BookingManagerComponent,
    CalendarComponent,
    MembersComponent,
    SearchFilterPipe,
  ],
  imports: [
    NzSelectModule,
    NzDatePickerModule,
    NzTimePickerModule,
    BrowserModule,
    ApproutingModule,
    BrowserAnimationsModule,
    MatMenuModule,
    HttpClientModule,
    MatExpansionModule,
    MatSnackBarModule,
    MatButtonModule,
    ReactiveFormsModule,
    FormsModule,
    NzInputModule
  ],
  providers: [{ provide: NZ_I18N, useValue: en_US }],
  bootstrap: [AppComponent],
})
export class AppModule {
}
