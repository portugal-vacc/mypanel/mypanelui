import { AfterViewInit, Component, Input } from '@angular/core';
import { AppUtils } from '../appUtils';

@Component({
  selector: 'app-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.scss']
})
export class AvatarComponent implements AfterViewInit {
 

  @Input('name') userName : string;
  @Input('size') circleSize : string;

  init = false;

  // to access in html
  AppUtils = AppUtils;


  ngAfterViewInit(): void {
    this.init = true; // 
  }
}
