import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, finalize } from 'rxjs';
import { NavigationService } from '../navigation.service';
import { HttpService } from '../http.service';
import { Booking } from '../model/model';
import { BookingType, ControllerPositions, NetworkChoices } from '../model/enums';
import { AuthService } from '../auth.service';
import { AppUtils } from '../appUtils';

@Component({
    selector: 'app-booking-manager',
    templateUrl: './booking-manager.component.html',
    styleUrls: ['./booking-manager.component.scss']
})
export class BookingManagerComponent implements OnInit, OnDestroy {

    subs: Subscription[] = [];

    Controller_callsign: string[] = [];

    editMode = false;

    submitInProgress = false;

    BookingIdEdit: string;
    booking: Booking = {} as Booking;


    pickedDate : Date;
    
        endTime: Date;
    startTime: Date;


    auxStartTime : Date;
    auxEndTime : Date;

    // vars for calendar limits
    private readonly today  = new Date(Date.now());
    private readonly maxDate = new Date(new Date().getFullYear() + 1, 11, 31);

    selectedCallsign : string;

    submitted: boolean;

    time: Date;
    defaultOpenValue : Date;

    appUtils: AppUtils;

    cid: number;

    constructor(
        private httpService: HttpService,
        private route: ActivatedRoute,
        private navigationService: NavigationService,
        private router: Router,
        private authService: AuthService
    ) {
    }

    ngOnDestroy(): void {
        this.subs.forEach((s) => s.unsubscribe());
    }


    ngOnInit() {
        this.cid = this.authService.getCID(); 

        // set all zeros, so when picker starts its all 0 0 0 0 
        this.defaultOpenValue = new Date(0, 0, 0, 0, 0, 0);
       
        this.subs.push(
            this.route.data.subscribe((data) => {
                this.navigationService.emitTitleChange(data['title']);
            })
        );

        const type = this.route.snapshot.paramMap.get('type');
        if (!type || (type !== 'edit' && type !== 'new')) {
            this.router.navigate(['']);
        }

        this.BookingIdEdit = this.route.snapshot.paramMap.get('bookingId');
        if (this.BookingIdEdit) {
            this.editMode = true;
            this.httpService.getBookingById(this.BookingIdEdit).subscribe({
                next: (res) => {
                    this.setFormDetails(res);
                },
                error: () => {
                    alert('Error: Could not fetch booking number ' + this.BookingIdEdit);
                }
            });
        }

        this.setDropDownControllerPositions();
    }


    submit() {

        if(this.submitInProgress) {
            return;
        }
        // helper variable to dprevent multiple button presses
        this.submitInProgress = true;

        // helper variable to enable verification/red outline on fields
        this.submitted = true;
        if (this.editMode) {
            this.editBooking();
        } else {
            this.createBooking();
        }
    }

    createBooking() {
        this.autocompleteBooking();
        if (this.validate()) {
            this.booking.client_id = this.cid;
            this.booking.network = NetworkChoices.live;
            this.booking.type = BookingType.booking;

            this.httpService.createBooking(this.booking).pipe(finalize( () => this.submitInProgress = false)).subscribe(
                {
                    next: () => {
                        console.warn('Your booking has been created');
                        this.router.navigate(['dashboard']);
                    },
                    error: (e) => {
                        alert('Error on completing the request ' + e);
                    },
                    
                }
            );
        } else {
            this.submitInProgress = false;
        }
    }


    editBooking() {
        this.autocompleteBooking();
        if (this.validate()) {
            this.httpService.updateBooking(this.booking).pipe(finalize( () => this.submitInProgress = false)).subscribe(
                {
                    next: () => {
                        console.warn('Your booking has been edited');
                        this.router.navigate(['dashboard']);
                    },
                    error: (e) => {
                        alert('Error on completing the request ' + e);
                    },
                    
                }
            );
        } else {
            this.submitInProgress = false;
        }
    }


    private validate() {
        // TODO, booleans to affect 
        if (!this.startTime || !this.endTime || !this.selectedCallsign) {
            return false;
        }

        const now = new Date();
        if (this.auxStartTime <= now || this.auxEndTime <= now) {
            return false;
        }

        return true;
    }

    private autocompleteBooking() {
        if (this.pickedDate && this.startTime && this.endTime) {

            const plannedDay = this.pickedDate;
            const startDateTime = new Date(this.startTime.setUTCFullYear(plannedDay.getUTCFullYear(), plannedDay.getUTCMonth(), plannedDay.getDate()));
            const endDateTime = new Date(this.endTime.setUTCFullYear(plannedDay.getUTCFullYear(), plannedDay.getUTCMonth(), plannedDay.getDate()));

            // affect aux variables for field validation. Do not change the originals otherwise it will change in page, due to  data binding (ngmodel)
            this.auxStartTime = startDateTime;
            this.auxEndTime = endDateTime;

            // convert to generate a date with UTC instead of local time
            this.booking.start_datetime = AppUtils.convertLocalDateToUTC(startDateTime);
            this.booking.end_datetime = AppUtils.convertLocalDateToUTC(endDateTime);

            // IF end date is after start date. Send it a day forward. For e.g. STart 2300Z, END 0000Z
            if (this.booking.end_datetime < this.booking.start_datetime) {
                this.booking.end_datetime = new Date(this.booking.end_datetime.setDate(this.booking.end_datetime.getDate() + 1));
            }
            this.booking.call_sign = this.selectedCallsign;
        }
    }

    private setDropDownControllerPositions() {
        for (const key in ControllerPositions) {
            this.Controller_callsign.push(key);
        }
    }

    private setFormDetails(booking: Booking) {
        if (booking) {
            this.booking = booking;
            // Form uses local time, convert date from UTC to a local date time with the same hour.
            this.startTime = AppUtils.convertUtcToLocalDate(booking.start_datetime);
            this.endTime = AppUtils.convertUtcToLocalDate(booking.end_datetime);
            this.selectedCallsign = this.booking.call_sign;
            this.pickedDate = new Date(new Date(booking.start_datetime).setHours(0, 0, 0, 0));

        }
    }


    // do not use normal function to prevent undefined today behaviour.
    // Disable dates are the ones which the time has past (difference in days less than zero)
    disabledDate = (current: Date): boolean => 
        current > this.maxDate || AppUtils.getDifferenceInDays(current, this.today) < 0;
    

}
