import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from '../auth.service';
import { IconTypeEnum } from '../model/enums';
import { HttpService } from '../http.service';
import { NavigationService } from '../navigation.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {

  private subs : Subscription[] = [];

  // plaveholders to fetch from http
  protected notifications : string[] = ['OLA', ' DOIS', 'TRES', 'QUATRO', 'CINCO', 'SEIS'];

  protected title = '';

  protected IconTypeEnum = IconTypeEnum;

  protected year : number;

  // placehoders to fetch from appService/AuthService
  protected name : string;
  protected rating : string;

  protected seed : number;

  protected notificationsOpen: boolean;

  protected notificationsUnread = true;

  constructor(public authService : AuthService, private httpService : HttpService, private navigation : NavigationService, private snackBar: MatSnackBar) {

  }
  ngOnInit(): void {
    this.name = 'ZE ZE';
    this.rating = 'TMA Controller';
    this.year = new Date().getFullYear();
    this.subs.push(this.navigation.getTitleObservable().subscribe(title => {
      this.title = title;
    }));
  
    this.seed = Math.random() * 10000;
  }

    ngOnDestroy(): void {
     this.subs.forEach(sub => {
      sub.unsubscribe();
     });
  }

  navigateNotifications() {
    this.notificationsOpen = false;
    this.navigation.navigateTo('/user/notifications');
  }

  clearNotifications() {
    this.notificationsUnread= false;
    window.alert('TO implement');
  }


  openNotifications() {
    this.notificationsOpen = !this.notificationsOpen;
  }
  logOut() {
    this.authService.logOut();
  }

}
