import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable, throwError } from 'rxjs';
import { AppConfig } from './app-config';
import { AuthService } from './auth.service';
import { BookingType, NetworkChoices } from './model/enums';
import { ApiFilter, AuthData, Booking, VatsimEvent } from './model/model';
import { IOBooking, IOEvent } from './model/model-io';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient, private authService: AuthService, private matSnackBar : MatSnackBar) { }


  errorHandler(error: HttpErrorResponse) {

    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);

      if (error.status === 401) {
        this.matSnackBar.open('Registration invalid, please login again!', 'Close');
        this.authService.logOut();
      }
    }
    // return an observable with a user-facing error message
    return throwError(() => new Error());
  }

  private getHttpOptions() {
    const headers = new HttpHeaders({
        'AuthoRization': 'Bearer ' + this.authService.getToken(),
        'Content-Type': 'Application/Json'
      });
      const options = {
        'headers' : headers
      };
      return options;
    }

  private getHttpHeaders(): HttpHeaders {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.authService.getToken(), // TODO, confirmar se o bearer vem no token ou adicionar aqui
    });
    return headers;
  }


  /** AUTH */

  validateToken(token : string) : Observable<AuthData> {

    return this.http.get<AuthData>(AppConfig.API_BASE_URL + AppConfig.API_AUTH_HREF + '/active', {
      params : {
        'token' : token
      }
    }).pipe(
      catchError(this.errorHandler)
    );
  }

  logout() {
    return this.http.get(AppConfig.API_BASE_URL + AppConfig.API_AUTH_HREF + '/logout', this.getHttpOptions()).pipe(
      catchError(this.errorHandler));

  }

  /** BOOKINGS */

  getBookings(filter: ApiFilter): Observable<Booking[]> {

    return this.http.get<IOBooking[]>(AppConfig.API_BASE_URL + AppConfig.API_BOOKING_HREF, {
      params: {
        skip: filter ? filter.skip : 0,
        limit: filter ? filter.limit : 50
      },
      headers: this.getHttpHeaders()
    })
      .pipe(
        map((b) => {
          return b.map(io => this.mapBooking(io));
        }),
        catchError(this.errorHandler)
      );
  }

  getFutureBookings(): Observable<Booking[]> {

    return this.http.get<IOBooking[]>(AppConfig.API_BASE_URL + AppConfig.API_BOOKING_HREF + '/future', this.getHttpOptions())
      .pipe(
        map((b) => {
          return b.map(io => this.mapBooking(io));
        }),
        catchError(this.errorHandler)
      );
  }

  getClientBookings(userid: number): Observable<Booking[]> {

    return this.http.get<IOBooking[]>(AppConfig.API_BASE_URL + AppConfig.API_BOOKING_HREF + '/client/' + userid, this.getHttpOptions())
      .pipe(
        map((b) => {
          return b.map(io => this.mapBooking(io));
        }),
        catchError(this.errorHandler)
      );
  }

  getMentorBookings(mentorid: number) {
    return this.http.get<IOBooking[]>(AppConfig.API_BASE_URL + AppConfig.API_BOOKING_HREF + '/mentor/' + mentorid, this.getHttpOptions())
      .pipe(
        map((b) => {
          return b.map(io => this.mapBooking(io));
        }),
        catchError(this.errorHandler)
      );
  }

  createBooking(booking: Booking) {
    const body = this.createBookingBody(booking);
    return this.http.post(AppConfig.API_BASE_URL + AppConfig.API_BOOKING_HREF, body, this.getHttpOptions()).pipe(
      catchError(this.errorHandler)
    );
  }

  updateBooking(booking: Booking) {
    const body = this.createBookingBody(booking);
    return this.http.patch(AppConfig.API_BASE_URL + AppConfig.API_BOOKING_HREF + '/' + booking.id, body, this.getHttpOptions()).pipe(
      catchError(this.errorHandler)
    );
  }

  deleteBooking(booking: Booking) {
    return this.http.delete(AppConfig.API_BASE_URL + AppConfig.API_BOOKING_HREF + '/' + booking.id, this.getHttpOptions()).pipe(
      catchError(this.errorHandler)
    );
  }

  getEvents() :Observable<VatsimEvent[]> {
    return this.http.get<IOEvent[]>(AppConfig.API_EVENTS_REF, {
      params: {
        skip: 0,
        limit: 50
      },
      headers: this.getHttpHeaders()
    })
      .pipe(
        map((b) => {
          return b.map(io => this.mapEvent(io));
        }),
        catchError(this.errorHandler)
      );
  }

  getBookingById(bookingId: string) {
    return this.http.get<IOBooking>(AppConfig.API_BASE_URL + AppConfig.API_BOOKING_HREF + '/' + bookingId, this.getHttpOptions()).pipe(
      map((io) => {
        return this.mapBooking(io);
      }),
      catchError(this.errorHandler)
    );
  }


  /** BODY PARSING  */

  mapBooking(io: IOBooking): Booking {
    return {
      id: io.id,
      client_id: io.client_id,
      call_sign: io.call_sign,
      mentor_client_id: io.mentor_client_id,
      start_datetime: new Date(io.start_datetime),
      end_datetime: new Date(io.end_datetime),
      network: NetworkChoices[(io.network as any) as keyof typeof NetworkChoices],
      type: BookingType[(io.type as any) as keyof typeof BookingType]
    };
  }

  mapEvent(io: IOEvent): VatsimEvent {
    return {
      id: io.id,
      type: io.type,
      name: io.name,
      link: io.link,
      airports : io.airports,
      routes: io.routes,
      start_time: io.start_time,
      end_time: io.end_time,
      short_description: io.short_description,
      description: io.description,
      banner: io.banner,
    };
  }


  /** BODY CREATION */

  createBookingBody(booking: Booking): IOBooking {
    let network: string;
    let type: string;
    Object.keys(NetworkChoices).forEach(k => {
      const val: string = NetworkChoices[k as unknown as keyof typeof NetworkChoices];
      if (val === booking.network) {
        network = k;
      }
    });

    Object.keys(BookingType).forEach(k => {
      const val: string = BookingType[k as unknown as keyof typeof BookingType];
      if (val === booking.type) {
        type = k;
      }
    });
    return {
      client_id: booking.client_id,
      mentor_client_id: booking.mentor_client_id,
      call_sign: booking.call_sign,
      end_datetime: booking.end_datetime? booking.end_datetime.toISOString() : null,
      start_datetime: booking.start_datetime? booking.start_datetime.toISOString() : null,
      type: type,
      network: network,
    };
  }

}
