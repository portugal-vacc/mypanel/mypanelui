import { Component, OnInit } from '@angular/core';
import {Router } from '@angular/router';
import { AppConfig } from '../app-config';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {

  AppConfig = AppConfig;

  href : string;

  constructor(private authService : AuthService, private router : Router) {
  }
  ngOnInit(): void {
    this.href = AppConfig.API_BASE_URL +'/auth/login';
    if(this.authService.isLoggedIn()) {
      this.router.navigate(['dashboard']);  
    }
  }

  

  login() {
    // hardcoded JWT with CID: 100002, reimplement when passport
    this.authService.storeToken('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwiY2xpZW50X2lkIjoiMTAwMDAwMDIiLCJleHAiOjE1MTYyMzkwMjJ9.RXxP8S_aKf0R4gMA_Ic1hAjFwQxqK3qOXOGGdiu32i4');
    this.router.navigate(['dashboard']);
  }

}
