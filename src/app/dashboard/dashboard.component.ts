import {Component, OnDestroy, OnInit} from '@angular/core';
import {HttpService} from '../http.service';
import {BookingType, NetworkChoices} from '../model/enums';
import {ApiFilter, Booking, VatsimEvent} from '../model/model';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {NavigationService} from '../navigation.service';
import {AuthService} from '../auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit, OnDestroy {
  bookings: Booking[] = [];
  events: VatsimEvent[] = [];
  userBookings: Booking[] = [];
  errorEvents: boolean;
  errorBookings: boolean;
  cid: number;

  subs: Subscription[] = [];

  filter: ApiFilter = {
    limit: 50,
    skip: 0,
  };

  constructor(
    private httpService: HttpService,
    private route: ActivatedRoute,
    private navigationService: NavigationService,
    private router: Router,
    private authService: AuthService
  ) {
  }

  ngOnDestroy(): void {
    this.subs.forEach((s) => s.unsubscribe());
  }

  ngOnInit(): void {
    this.cid = Number(this.authService.getCID());
    this.subs.push(
      this.route.data.subscribe((data) => {
        this.navigationService.emitTitleChange(data['title']);
      })
    );
    this.refreshBookings();
    this.refreshEvents();
  }

  refreshBookings() {
    this.httpService.getBookings(this.filter).subscribe({
      next: (bookings) => {
        this.errorBookings = false;
        this.bookings = bookings.filter(
          (booking) => {
            const today = new Date();
            const tomorrow = new Date(today);
            tomorrow.setDate(today.getDate() + 50);
            const bookingDate = new Date(booking.start_datetime);
            return bookingDate >
              today && bookingDate <
              tomorrow && booking.network === NetworkChoices.live;
          }
        ).sort((a, b) => {
          const aDate = a.start_datetime;
          const bDate = b.start_datetime;
          return aDate > bDate ? 1 : aDate < bDate ? -1 : 0;
        });

        this.userBookings = bookings.filter(
          (booking) => {
            return booking.client_id === this.cid && booking.network === NetworkChoices.live;
          }
        ).sort((a, b) => {
          const aDate = a.start_datetime;
          const bDate = b.start_datetime;
          return aDate > bDate ? 1 : aDate < bDate ? -1 : 0;
        });

      },
      error: () => {
        this.errorBookings = true;
      },
    });
  }

  refreshEvents() {
    this.httpService.getEvents().subscribe({
      next: events => {
        this.events = events.filter(
          (event) => {
            const today = new Date();
            const filterDate = new Date(today);
            filterDate.setDate(today.getDate() + 45);
            return new Date(event.start_time) < filterDate;
          });
        this.errorEvents = false;
      },
      error: () => {
        this.errorEvents = true;
      },
    });
  }

  createBooking() {
    const date: Date = new Date('2023-10-15 10:00Z');
    const endDate: Date = new Date('2023-10-15 12:00Z');

    const booking: Booking = {
      client_id: 1,
      start_datetime: date,
      end_datetime: endDate,
      network: NetworkChoices.live,
      type: BookingType.event,
      call_sign: 'Zé Zé Tozé',
      mentor_client_id: 0,
    };
    this.httpService.createBooking(booking).subscribe(() => {
      this.refreshBookings();
    });
    const selectedForm = document.getElementById('form');
    selectedForm.classList.remove('hidden-booking-form');
  }

  deleteBooking(booking: Booking) {
    this.httpService.deleteBooking(booking).subscribe({
      error: (e) => alert('Error on completing the request ' + e),
      next: () => this.refreshBookings()
    });
  }

  updateBooking() {
    if (this.bookings.length !== 0) {
      const booking: Booking = JSON.parse(
        JSON.stringify(this.bookings[this.bookings.length - 1])
      );
      booking.type = BookingType.exam;
      booking.network = NetworkChoices.sweatbox;
      this.httpService.updateBooking(booking).subscribe(() => {
        this.refreshBookings();
      });
    }
  }


  editBooking(booking: Booking) {
    this.router.navigate(['/booking-manager/edit/' + booking.id]);
  }

}
