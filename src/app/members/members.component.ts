import {Component} from '@angular/core';
import {Subscription} from 'rxjs';
import {HttpService} from '../http.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NavigationService} from '../navigation.service';
import {AuthService} from '../auth.service';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.scss']
})
export class MembersComponent {

  subs: Subscription[] = [];

  colNumber = 4;
  selectedPage = 1;
  maxPageNumber = 3;

  searchMembers = '';

  // Only for testing
  // TODO - use variable with response from API
  membersList = [{
    cid: 1400951, name: 'Alexandre Nogueira', rating: 'S3', certifications: ['LPPT', 'LPMA']
  }, {
    cid: 100001, name: 'Guilherme Macedo', rating: 'S3', certifications: ['LPPT', 'LPMA']
  }, {
    cid: 100002, name: 'Tiago Cunha', rating: 'S3', certifications: ['LPPT', 'LPMA']
  }, {
    cid: 100003, name: 'Bruno', rating: 'S3', certifications: ['LPPT', 'LPMA']
  }];

  constructor(
    private httpService: HttpService,
    private route: ActivatedRoute,
    private navigationService: NavigationService,
    private router: Router,
    private authService: AuthService) {
  }

  ngOnDestroy(): void {
    this.subs.forEach((s) => s.unsubscribe());
  }

  ngOnInit() {
    this.subs.push(
      this.route.data.subscribe((data) => {
        this.navigationService.emitTitleChange(data['title']);
      })
    );
  }

  getMembers() {
    return;
  }

  previousPage() {
    this.selectedPage--; // change to match the way the pages are stored
  }

  nextPage() {
    this.selectedPage++; // change to match the way the pages are stored
  }

  changePage(pageId: number) {
    this.selectedPage = pageId;
  }

}
