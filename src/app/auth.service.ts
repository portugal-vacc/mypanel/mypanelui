import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AppConfig } from './app-config';
import { AuthData } from './model/model';

@Injectable({
   providedIn: 'root'
 })
export class AuthService{

     
   private userData : AuthData;

    constructor(private router : Router) {

    }

     storeToken(token : string) {
        localStorage.setItem(AppConfig.TOKEN_KEY, token);
        this.parseAndStoreJWT(token);
     }

     getToken() {
      return localStorage.getItem(AppConfig.TOKEN_KEY);
     }

     logOut() {
        localStorage.removeItem(AppConfig.TOKEN_KEY);
        this.userData = null;
        this.router.navigate(['login']);
     }

     isLoggedIn() {
        const token = localStorage.getItem(AppConfig.TOKEN_KEY);
        const loggedIn = token !== null && token !== undefined && token.length !== 0;
        if(loggedIn && !this.userData) {
         this.parseAndStoreJWT(token);
        }
        return loggedIn;
     }

     private parseAndStoreJWT(jwt : string) {
         this.userData = JSON.parse(atob(jwt.split('.')[1])); // TODO remove atob/refactor when passport
     }

   getCID() :number {
      if(this.userData) {
         return this.userData.client_id;
      }
      return null;
     }

}