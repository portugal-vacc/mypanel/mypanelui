import { Component, Input } from '@angular/core';
import { IconTypeEnum } from '../model/enums';

@Component({
  selector: 'app-svg-icon',
  templateUrl: './svg-icon.component.html',
  styleUrls: ['./svg-icon.component.scss']
})
export class SvgIconComponent {

  @Input('icon') iconName : IconTypeEnum;

  @Input('width') width : string;

  @Input('height') height : string;

  @Input('fill') fill : string;

  @Input('stroke') stroke : string;

  // just to access in html
  
  IconTypeEnum = IconTypeEnum;

}
