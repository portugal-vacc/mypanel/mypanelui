import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {canActivate, canActivateChild} from './app.guard';
import {CallbackComponent} from './callback/callback.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {HeaderComponent} from './header/header.component';
import {LoginComponent} from './login/login.component';
import {NotfoundComponent} from './notfound/notfound.component';
import {UserprofileComponent} from './userprofile/userprofile.component';
import {BookingManagerComponent} from './booking-manager/booking-manager.component';
import {CalendarComponent} from './calendar/calendar.component';
import {MembersComponent} from './members/members.component';


const routes: Routes = [

  {
    path: 'login', component: LoginComponent
  },

  {
    path: 'callback', component: CallbackComponent
  },
  {
    path: '', component: HeaderComponent, canActivate: [canActivate], canActivateChild: [canActivateChild],
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent,
        data: {title: 'Dashboard'}
      }, {
        path: 'user',
        component: UserprofileComponent,
        data: {title: 'userProfile'}
      },
      {
        path: 'booking-manager/:type',
        component: BookingManagerComponent,
        data: {title: 'BookingManager'}
      },
      {
        path: 'booking-manager/:type/:bookingId',
        component: BookingManagerComponent,
        data: {title: 'BookingManager'}
      },
      {
        path: 'calendar',
        component: CalendarComponent,
        data: {title: 'CalendarComponent'}
      },
      {
        path: 'calendar/:filter',
        component: CalendarComponent,
        data: {title: 'Calendar'}
      },
      {
        path: 'members',
        component: MembersComponent,
        data: {title: 'Members'},
      },
      {
        path: '**',
        component: NotfoundComponent
      }
    ]
  },
  {
    path: '**',
    component: LoginComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class ApproutingModule {


}
