export class AppConfig {
  /** DEV **/
  static readonly APP_URL = 'localhost:4200';

  static readonly API_BASE_URL = 'https://mypanel.vatsim.pt/api/v1';

  /** PROD  **/
  //static readonly APP_URL = 'mypanel.vatsim.pt';

  // static readonly API_BASE_URL = 'https://mypanel.vatsim.pt/api/v1';

  static readonly API_BOOKING_HREF = '/bookings';

  static readonly API_EVENTS_REF = 'https://vatdata.vatsim.pt/api/v1/events/portugal';

  static readonly API_AUTH_HREF = '/auth';

  static readonly TOKEN_KEY = 'My Panel Token';
}
