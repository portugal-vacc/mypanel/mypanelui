import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subject, delay } from  'rxjs';

@Injectable({
    providedIn: 'root'
  })

export class NavigationService {

    private titleSubject : Subject<string> = new Subject<string>;

    constructor(private router: Router) {

    }

    // cannot grab data directly from angular router in header component since data is passed to others. As such, we use this helper service
    public emitTitleChange(title : string) {
        this.titleSubject.next(title);
    }

    public getTitleObservable(): Observable<string> {
        return this.titleSubject.pipe(delay(0)); // delay 0 to prevent error : ExpressionChangedAfterItHasBeenCheckedError: Expression has changed after it was checked: https://stackoverflow.com/a/63230254
    }

    navigateTo(url : string) {
        this.router.navigate([url]);
    }
}