import {Component} from '@angular/core';
import {HttpService} from '../http.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NavigationService} from '../navigation.service';
import {ApiFilter, Booking, VatsimEvent} from '../model/model';
import {first} from 'rxjs';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent {

  calendarFilter: string;

  weekDays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  currMonthEntries: {
    dayNumber: number,
    entries: { name: string, start_time: string, end_time: string, link: string }[]
  }[] = [];

  selectedDate = new Date();
  selMonth: number;
  selYear: number;

  filter: ApiFilter = {
    limit: 50,
    skip: 0,
  };

  bookings: Booking[] = [];
  events: VatsimEvent[] = [];

  constructor(
    private httpService: HttpService,
    private route: ActivatedRoute,
    private navigationService: NavigationService,
    private router: Router,
  ) {
  }

  ngOnInit() {
    this.calendarFilter = this.route.snapshot.paramMap.get('filter');
    if (this.calendarFilter === 'bookings') {
      this.getBookings();
    }
    else if (this.calendarFilter === 'events') {
      this.getEvents();
    }
    else {
      this.getBookings();
      this.getEvents();
    }
  }

  getBookings() {
    this.httpService.getBookings(this.filter).subscribe(
      {
        next: bookings => {
          this.bookings = bookings;
          this.refreshCalendar();
        }
      });
  }

  getEvents() {
    this.httpService.getEvents().subscribe(
      {
        next: events => {
          this.events = events;
          this.refreshCalendar();
        }
      });
  }


  refreshCalendar() {
    this.selMonth = this.selectedDate.getMonth();
    this.selYear = this.selectedDate.getFullYear();
    const lastDayOfMonth = new Date(this.selectedDate.getFullYear(), this.selectedDate.getMonth() + 1, 0).getDate();
    const firstDayOfMonth = new Date(this.selectedDate.getFullYear(), this.selectedDate.getMonth(), 1).getDay();
    this.selectCurrMonthEntries(lastDayOfMonth, firstDayOfMonth);
  }

  selectCurrMonthEntries(lastDay: number, firstDay: number) {
    const currMonthBookings = this.bookings.filter((booking) => {
      return new Date(booking.start_datetime).getMonth() === this.selMonth;
    });

    const currMonthEvents = this.events.filter((events) => {
      return new Date(events.start_time).getMonth() === this.selMonth;
    });

    this.currMonthEntries = [];

    for (let i = firstDay; i > 0; i--) {
      this.currMonthEntries.push({dayNumber: null, entries: null});
    }

    for (let i = 0; i < lastDay; i++) {
      const obj = {
        dayNumber: i + 1,
        entries: [],
      };

      currMonthBookings.forEach(booking => {
        const bookingStartDate = new Date(booking.start_datetime);
        if (bookingStartDate.getDate() === i + 1 && bookingStartDate.getFullYear() === this.selYear) {
          obj.entries.push({
            name: booking.call_sign,
            start_time: booking.start_datetime,
            end_time: booking.end_datetime,
            link: '',
          });
        }
      });

      currMonthEvents.forEach(event => {
        const eventStartDate = new Date(event.start_time)
        if (eventStartDate.getDate() === i + 1 && eventStartDate.getFullYear() === this.selYear) {
          obj.entries.push({
            name: event.name,
            start_time: event.start_time,
            end_time: event.end_time,
            link: '',
          });
        }
      });

      obj.entries.sort((a, b) => {
        const aDate = a.start_time;
        const bDate = b.end_time;
        return aDate > bDate ? 1 : aDate < bDate ? -1 : 0;
      });

      this.currMonthEntries.push(obj);
    }
  }

  nextMonth() {
    this.selectedDate = new Date(this.selYear, this.selMonth + 1);
    this.refreshCalendar();
  }

  previousMonth() {
    this.selectedDate = new Date(this.selYear, this.selMonth - 1);
    this.refreshCalendar();
  }

  selectToday() {
    this.selectedDate = new Date();
    this.refreshCalendar();
  }
}
