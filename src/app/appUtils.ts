export class AppUtils {

    static readonly milisecondsInDay = 1000 * 60 * 60 * 24;

    static getNameInitials(name: string) {
        const splitName = name.split(' ');
        if (splitName.length < 2) {
            return splitName[0].charAt(0).toUpperCase();
        } else {
            return splitName[0].charAt(0).toUpperCase() + splitName[1].charAt(0).toUpperCase();
        }
    }

    /**
     * 
     * @param date to be converted
     * @returns a date string in format yyyy-mm--dd
     */
    static convertDateToYYYYMMDD(date: Date): string {
        return date.toISOString().split('T')[0];
    }


    /**
     *  Converts a local date to utc timestamp by subratcting the local date timeZoneOffset(negative if ahead of UTC, positive otherwise).
     *  Resulting date will be shifted so a UTC can be generated. 1200Local (UTC +1 ) -> 1100UTC
     * @param date date not in UTC with offset
     * @returns a date object with 0 offset
     */
    static convertLocalDateToUTC(date: Date): Date {
        if (date.getTimezoneOffset() === 0) {
            return date;
        }
        return new Date(date.setMinutes(date.getMinutes() - date.getTimezoneOffset()));
    }

    /**
 *  Converts a utc date to a local date by adding the timezone offset(negative if ahead of UTC, positive otherwise).
 *  Resulting date will have the same hour as the UTC but in local time instead. 1000Z  -> 1000Local
 * @param date date in UTC
 * @returns a date object converted to localDate 
 */
    static convertUtcToLocalDate(date: Date): Date {
        if (date.getTimezoneOffset() === 0) {
            return date;
        }
        return new Date(date.setMinutes(date.getMinutes() + date.getTimezoneOffset()));
    }

    /**
     * Calculates the difference in days between two dates by converting them to UTC and making the difference between the two. Then divide
     * by nr of miliseconds in a day to get the day.
     * @returns the difference in days between the two dates
     */

    static getDifferenceInDays(first: Date, second: Date): number {
        const firstUtc = Date.UTC(first.getFullYear(), first.getMonth(), first.getDate());
        const secondUtc = Date.UTC(second.getFullYear(), second.getMonth(), second.getDate());
        return Math.floor((firstUtc - secondUtc) / this.milisecondsInDay);
    }

}