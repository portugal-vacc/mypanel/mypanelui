export enum IconTypeEnum {

    // header Icons
    SETTINGS,
    NOTIFICATION,
    DROPDOW,
    SEARCH,
    // navigation Icons
    HOME,
    CALENDAR,
    DONATE,
    MEMBERS,
    DISCORD,
    DOWNLOADS,
    LOGOUT
}

export enum BookingType {
    booking = 'Booking',
    training = 'Training',
    event = 'Event',
    exam ='Exam'
}

export enum NetworkChoices{
    live = 'VATSIM',
    sweatbox = 'SweatBox'
}

export enum ControllerPositions {
  LPPT_DEL = 'LPPT_DEL',
  LPPT_GND = 'LPPT_GND',
  LPPT_TWR = 'LPPT_TWR',
  LPPT_APP = 'LPPT_APP',
  LPPT_U_APP = 'LPPT_U_APP',
  LPPT_F_APP = 'LPPT_F_APP',
  LPPR_DEL = 'LPPR_DEL',
  LPPR_TWR = 'LPPR_TWR',
  LPFR_GND = 'LPFR_GND',
  LPFR_TWR = 'LPFR_TWR',
  LPCS_TWR = 'LPCS_TWR',
  LPMA_TWR = 'LPMA_TWR',
  LPMA_APP = 'LPMA_APP',
  LPPC_CTR = 'LPPC_CTR',
  LPPC_E_CTR = 'LPPC_E_CTR',
  LPPC_W_CTR = 'LPPC_W_CTR',
  LPPC_N_CTR = 'LPPC_N_CTR',
  LPPC_C_CTR = 'LPPC_C_CTR',
  LPPC_S_CTR = 'LPPC_S_CTR',
  LPPC_I_CTR = 'LPPC_I_CTR',
}
