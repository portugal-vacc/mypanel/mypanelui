import { BookingType, NetworkChoices } from './enums';

export interface Booking {
    id? : number,
    client_id : number,
    network : NetworkChoices,
    type : BookingType,
    mentor_client_id? : number,
    call_sign : string,
    start_datetime : Date,
    end_datetime : Date,
}

export interface VatsimEvent {
  id: number,
  type: string,
  name: string,
  link: string,
  airports : {
    icao: string,
  }[],
  routes: any[],
  start_time: Date,
  end_time: Date,
  short_description: string,
  description: string,
  banner: string,
}


export interface ApiFilter {

    skip : number,
    limit : number
}

export interface AuthData {
    client_id : number,
    exp : number
}
