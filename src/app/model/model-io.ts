export interface IOBooking {

    client_id : number,
    network : string,
    type : string,
    mentor_client_id?: number,
    call_sign : string,
    start_datetime : string,
    end_datetime : string,
    id? : number,
    created_at? : string,
    last_updated? : string
}
export interface IOEvent {
  id: number,
  type: string,
  vso_name: string,
  name: string,
  link: string,
  organizers: any,
  airports : {
    icao: string,
  }[],
  routes: any[],
  start_time: Date,
  end_time: Date,
  short_description: string,
  description: string,
  banner: string,
}
